
# We will need to import these.
import numpy as np
import matplotlib.pyplot as plt
#from spicy.interpolate import spline
# store data in NumPy arrays for later use
# (could also read data from a file using 'open')
teme = []
x = []
y = []
z = []
i = 1;
with open('vizualizacija_faza3.txt', 'r')  as f:
	for line in f:
		z.append(line.split())
teme.append('Nazivi tema')
for line in z:
	teme.append(str(i) + '.' + line[0])
	i = i + 1
	x.append(int(line[1]))
	y.append(int(line[2]))
print(x)
print(y)
print(teme)
text = '\n'.join(teme)
#textstr = text%(1,7)
theme = 1.0 + np.arange(len(y))
fig = plt.figure()
plt.text(-1, 400, text, fontsize=8)
# setup the plots                        
plt.plot(theme, y, 'bo-', label='prihvaceno')
plt.plot(theme, x, 'r^-', label='ponudjeno')
#plt.fill_between(theme,x, color='m', alpha=0.3)
#plt.fill_between(theme,y, color='w')
plt.xlim(1,7)
plt.grid()
plt.legend()
plt.xlabel('Teme')
plt.ylabel('Ukupan broj ponudjenih/prihacenih odgovora')
plt.title('Faza 3 output')
plt.subplots_adjust(left=0.25)
# now, make the plot alreqdy!
plt.show()
