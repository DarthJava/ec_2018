package popcorn.phase2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Map Class which extends MaReduce.Mapper class
 * Map is passed a single line at a time, it splits the line based on space
 * and generated the token which are output by map with value as one to be consumed
 * by reduce class
 *
 * @author Raman
 */
public class MapClass extends Mapper<LongWritable, Text, Text, IntWritable> {

  private final static IntWritable one = new IntWritable(1);
  private final static IntWritable number = new IntWritable(1);
  private Text word = new Text();

  List<String> getPhase1() {
    List<String> results = new ArrayList();
    try {
      URL oracle = new URL("https://s3.eu-central-1.amazonaws.com/bigdatachallenge/team-4/step-1-complete");
      BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));

      String inputLine;
      int counter = 0;
      while ((inputLine = in.readLine()) != null && counter < 500) {
        String[] value = inputLine.split("\t");
        results.add(value[0]);
        ++counter;
      }

      in.close();

    } catch (IOException e) {
      e.printStackTrace();
    }

    return results;
  }

  int findOccurances(String token, String input) {
    String str = input.toLowerCase();
    String findStr = token.toLowerCase();
    int lastIndex = 0;
    int count = 0;

    while(lastIndex != -1){

      lastIndex = str.indexOf(findStr.toLowerCase(),lastIndex);

      if(lastIndex != -1){
        count ++;
        lastIndex += findStr.length();
      }
    }
    return count;
  }
  /**
   * map function of Mapper parent class takes a line of text at a time
   * splits to tokens and passes to the context as word along with value as one
   */
  @Override
  protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

    if (value.toString().contains("<?xml version=\"1.0\" encoding=\"utf-8\"?>")
        || value.toString().contains("<posts>")
        || value.toString().contains("</posts>")) {
      return;
    }

    List<String> phase1 = getPhase1();


    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = null;
    try {
      dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(new InputSource(new StringReader(value.toString())));
      doc.getDocumentElement().normalize();
      NodeList nList = doc.getElementsByTagName("row");
      if (nList.item(0).getAttributes().getNamedItem("Body") != null) {
        String atts = nList.item(0).getAttributes().getNamedItem("Body").getTextContent();
        for(String pKey : phase1) {
          int count = findOccurances(pKey, atts);
          word.set(pKey);
          number.set(count);
          context.write(word, number);
        }
      }
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    }
  }
}
