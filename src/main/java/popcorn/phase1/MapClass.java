package popcorn.phase1;

import static popcorn.App.testInput;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.StringTokenizer;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Map Class which extends MaReduce.Mapper class
 * Map is passed a single line at a time, it splits the line based on space
 * and generated the token which are output by map with value as one to be consumed
 * by reduce class
 *
 * @author Raman
 */
public class MapClass extends Mapper<LongWritable, Text, Text, IntWritable> {

  private final static IntWritable one = new IntWritable(1);
  private Text word = new Text();

  /**
   * map function of Mapper parent class takes a line of text at a time
   * splits to tokens and passes to the context as word along with value as one
   */
  @Override
  protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

    if (value.toString().contains("<?xml version=\"1.0\" encoding=\"utf-8\"?>")
        || value.toString().contains("<posts>")
        || value.toString().contains("</posts>")) {
      return;
    }


    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = null;
    try {
      dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(new InputSource(new StringReader(value.toString())));
      doc.getDocumentElement().normalize();
      NodeList nList = doc.getElementsByTagName("row");
      if (nList.item(0).getAttributes().getNamedItem("Tags") != null) {
        String atts = nList.item(0).getAttributes().getNamedItem("Tags").getTextContent();
        String[] list = atts.split("<");
        for(String s : list) {context.write(word, one);
          word.set(s.replace(">", ""));
          context.write(word, one);
        }
      }
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    }
  }
}
