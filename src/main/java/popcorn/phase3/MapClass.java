package popcorn.phase3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Map Class which extends MaReduce.Mapper class
 * Map is passed a single line at a time, it splits the line based on space
 * and generated the token which are output by map with value as one to be consumed
 * by reduce class
 *
 * @author Raman
 */
public class MapClass extends Mapper<LongWritable, Text, Text, IntWritable> {

  private final static IntWritable one = new IntWritable(1);
  private Text word = new Text();
  private IntWritable rex = new IntWritable();

  List<String> getPhase2() {
    List<String> results = new ArrayList();
    try {
      URL oracle = new URL("https://s3.eu-central-1.amazonaws.com/bigdatachallenge/team-4/step-2-complete");
      BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));

      String inputLine;
      int counter = 0;
      while ((inputLine = in.readLine()) != null && counter < 50) {
        String[] value = inputLine.split("\t");
        results.add(value[0]);
        ++counter;
      }

      in.close();

    } catch (IOException e) {
      e.printStackTrace();
    }

    return results;
  }


  List<String> getBetaPhase2() {
    List<String> beta = new ArrayList<String>();
    beta.add("3d");
    beta.add("advice");
    beta.add("algorithms");
    beta.add("analysis");
    beta.add("applications");
    beta.add("characters");

    return beta;
  }

  int findOccurances(String token, String input) {
    String str = input.toLowerCase();
    String findStr = token.toLowerCase();
    int lastIndex = 0;
    int count = 0;

    while (lastIndex != -1) {

      lastIndex = str.indexOf(findStr.toLowerCase(), lastIndex);

      if (lastIndex != -1) {
        count++;
        lastIndex += findStr.length();
      }
    }
    return count;
  }

  /**
   * map function of Mapper parent class takes a line of text at a time
   * splits to tokens and passes to the context as word along with value as one
   */
  @Override
  protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

    if (value.toString().contains("<?xml version=\"1.0\" encoding=\"utf-8\"?>")
        || value.toString().contains("<posts>")
        || value.toString().contains("</posts>")) {
      return;
    }

    List<String> phase1 = getPhase2();


    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = null;
    try {
      dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(new InputSource(new StringReader(value.toString())));
      doc.getDocumentElement().normalize();
      NodeList nList = doc.getElementsByTagName("row");

      rex.set(-1);
      if (nList.item(0).getAttributes().getNamedItem("Body") != null) {
        String body = nList.item(0).getAttributes().getNamedItem("Body").getTextContent();
        for (String pKey : phase1) {
          int count = findOccurances(pKey, body);
          if (count > 0) {
            String outputDTO = "";
            String atts1 = nList.item(0).getAttributes().getNamedItem("PostTypeId").getTextContent();
            int type = Integer.parseInt(atts1);
            //outputDTO += (type);

            if(type == 1) {
              outputDTO += (34);
            }
            else if (type == 2) {
              outputDTO += (43);
            }

            if (nList.item(0).getAttributes().getNamedItem("AcceptedAnswerId") != null) {
              outputDTO += (1);
            } else {
              outputDTO += (0);
            }

            if (nList.item(0).getAttributes().getNamedItem("AnswerCount") != null) {
              Integer c = Integer.parseInt(nList.item(0).getAttributes().getNamedItem("AnswerCount").getTextContent());
              outputDTO += (c);
            } else {
              outputDTO += (0);
            }

            word.set(pKey);
            rex.set(Integer.parseInt(outputDTO));

          }
          context.write(word, rex);
        }
      }
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    }
  }
}
