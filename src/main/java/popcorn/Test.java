package popcorn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Test {
  static List<String> getPhase1() {
    List<String> results = new ArrayList();
    try {
      URL oracle = new URL("https://s3.eu-central-1.amazonaws.com/bigdatachallenge/team-4/step-1-complete");
      BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));

      String inputLine;
      int counter = 0;
      while ((inputLine = in.readLine()) != null && counter < 500) {
        String[] value = inputLine.split("\t");
        results.add(value[0]);
      }

      in.close();

    } catch (IOException e) {
      e.printStackTrace();
    }

    return results;
  }

  public static void main(String[] args) {

    List<String> results = getPhase1();

    for (String s : results) {
      System.out.println(s);
    }
  }

}